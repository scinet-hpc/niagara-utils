# Niagara SciNet Utilities

## Bash Utilities

These are in the bashutils subdirectory

`nodeperf`: 
 Performance details of the current node.

`jobperf` (SLURM specific):
 Performance details of each node involved in a given SLURM job.

`jobscript` (SLURM specific): 
 Prints the job script of a given SLURM job to screen.

`qsum` (SLURM specific)
 Aggregates SLURM's squeue output by user.

`modulewrap` (LMOD specific)
 Call a single command with a different set of modules loaded. 

`quota` (Niagara specific):
 Short summary of 'diskUsage' output.

`debugjob` (Niagara specific): 
 Get a short interactive job in the debug partition.

### Installation of bash utilities

    cd bashutils
    make
    make install PREFIX=[location]

Then ensure that `[location]` is in the path.

## Go Utilities

* `scinet`: currently only implements a single command,
  `scinet niagara priority`, which shows the current group's priority.

