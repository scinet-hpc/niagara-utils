#!/usr/bin/python -ESs
import sys
import os
import getpass

if not 'CLUSTER' in os.environ:
    print "Error: unknown and therefore unsupported cluster"
    sys.exit(1)

if os.environ['CLUSTER'] == 'niagara':
    MaxNodes = 4
    Minutes  = [0,60,45,30,22]
    Command  = ['/opt/slurm/bin/salloc','--ntasks-per-node=40','-pdebug']
    if 'DISPLAY' in os.environ and os.environ['DISPLAY'] != '':
        Command += ['--x11']

elif os.environ['CLUSTER'] == 'tds':
    MaxNodes = 2
    Minutes  = [0,60,30]
    Command  = ['/opt/slurm/bin/.xalloc','--ntasks-per-node=40','-pcompute']

elif os.environ['CLUSTER'] == 'teach':
    MaxNodes = 4
    Minutes  = [0,180,90,60,45]
    Command  = ['/opt/slurm/bin/.xalloc','--ntasks-per-node=16','-pcompute']

Nodes = 1
NodeTimeSpecArgOverride = False
if len(sys.argv)>1:
    if unicode(sys.argv[1], 'utf-8').isnumeric():
        Nodes = int(sys.argv[1])
        del sys.argv[1]
        if Nodes < 0 or Nodes > MaxNodes:
            print "Error: invalid number of nodes (max="+str(MaxNodes)+")."
            sys.exit(1)
    for i in xrange(len(sys.argv)):
        for NodeTimeArg in ['--time','-t','--nodes','-N']:
            if sys.argv[i] == NodeTimeArg or sys.argv[i].startswith(NodeTimeArg+'='):
                NodeTimeSpecArgOverride = True

time = Minutes[Nodes]
if time <= 60:
    Command += ['--time='+str(time)]
else:
    Command += ['--time='+str(time/60)+':'+str(time%60)+':00']

Command += ['--job-name=debugjob-'+str(Nodes)+'-'+getpass.getuser()]
Command += ['--nodes='+str(Nodes)]
Command += ['--exclusive']
Command += sys.argv[1:]

if not NodeTimeSpecArgOverride:
    print "debugjob: Requesting",Nodes,"nodes for",time,"minutes"

os.execv(Command[0], ['salloc'] + Command[1:])

