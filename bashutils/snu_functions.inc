# snu_functions.inc
#
# To be included by many of the SciNet bash utilities - specific for Niagara
#
# Ramses van Zon, SciNet, 2018-2019

set -u
export CLEANRUN_LD_LIBRARY_PATH="${LD_LIBRARY_PATH-}"
export LD_LIBRARY_PATH=

if [ -f ${mydir-}/snu.conf ]; then
   source ${mydir-}/snu.conf 
fi

# Map a name to a jobid
runningjobname2id()
{
    jobid=$($SACCT -n -X --format jobid --name "$1" --state running 2>/dev/null | tail -n 1)
    if [ -z "$jobid" ]; then
        jobid=$($SACCT -n -X --format jobid -j "$1" --state running 2>/dev/null | tail -n 1)
    fi
    echo "$jobid"
}

# Map a name to a jobid
jobname2id()
{
    jobid=$($SACCT -n -X --format jobid --name "$1" 2>/dev/null | tail -n 1)
    if [ -z "$jobid" ]; then
        jobid=$($SACCT -n -X --format jobid -j "$1" 2>/dev/null | tail -n 1)
    fi
    echo "$jobid"
}

# Undo shorthand range notation 
unrange() {
    while read line; do
        eval echo $(echo $line | $SED -e 's/\([0-9][0-9]*\)-\([0-9][0-9]*\)/{\1..\2}/g' -e 's/\[\(.*\),\(.*\)\]/{\1,\2}/g' ) | $TR ' ' '\n'
    done | $SED -e 's/\[\([0-9][0-9]*\)\]/\1/g'
}

# Draw a horizontal line.
hlinen()
{
    printf "%${1}s" | $TR ' ' '-'
}

# Draw a horizontal line.
hline()
{
    hlinen $1
    printf "\n"
}

# Draw a vertical line.
vline()
{    
    printf "|"
}

# Draw mix of horizontal vertical lines at the top of a table
topline()
{    
    while [ -n "$2" ]; do
        hlinen $1
        printf "+"
        shift
    done
    hlinen $1
    printf "\n"
}

# Draw mix of horizontal vertical lines at the bottom of a table
bottomline()
{    
    while [ -n "$2" ]; do
        hlinen $1
        printf "+"
        shift
    done
    hlinen $1
    printf "\n"
}

# Draw mix of horizontal vertical lines in the middle of a table
middleline()
{    
    while [ -n "$2" ]; do
        hlinen $1
        printf "+"
        shift
    done
    hlinen $1
    printf "\n"
}
