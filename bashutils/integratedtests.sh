#!/bin/bash
set -eu

echo "Testing nodeperf..."
./nodeperf > output.nodeperf
echo "Output in output.nodeperf"

echo "Testing modulewrap..."
./modulewrap -P gcc which gcc > output.modulewrap
echo "Output in output.modulewrap"

echo "Testing qsum..."
./qsum  > output.qsum
echo "Output in output.qsum"

echo "Testing quota..."
./quota  > output.quota
echo "Output in output.quota"

echo "Testing debugjob..."
./debugjob 2 sleep 20 2>&1 > output.debugjob &
sleep 10
echo "Output in output.debugjob"

if [ "${1-}" ]; then
    JOBID="$1"
else
    JOBID=$(squeue -hu $USER -o %i | tail -1)
fi

if [ -n "${JOBID-}" ]; then

    echo "Testing jobscript -l ..."
    ./jobscript -l "$JOBID"  > output.jobscript-l
    echo "Output in output.jobscript"

    echo "Testing jobscript ..."
    ./jobscript "$JOBID"  > output.jobscript
    echo "Output in output.jobscript"

    echo "Testing jobperf ..."
    ./jobperf "$JOBID"  > output.jobperf
    echo "Output in output.jobperf"

else

    echo "No jobid provided: not testing jobscript and jobperf "    

fi

sleep 20
wait
