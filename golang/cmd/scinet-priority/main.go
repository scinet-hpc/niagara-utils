package main

import (
	"fmt"
	"math"
	"os"
	"os/exec"
	"sort"
	"strconv"
	"strings"
	"text/tabwriter"
)

func isUser(username string) bool {
	return exec.Command("id", "-u", username).Run() == nil
}

func getDefaultAccountForUser(user string) (string, error) {
	cmd := exec.Command("sacctmgr", "-nP", "list", "user", user)
	out, err := cmd.Output()
	if err != nil {
		return "", err
	}
	cols := strings.Split(string(out), "|")
	if len(cols) < 2 || cols[0] != user {
		return "", fmt.Errorf("bad username: %s", user)
	}
	return cols[1], nil
}

type clusterInfo struct {
	name string
	size int
	unit string
}

func getClusterInfo() (info clusterInfo, err error) {
	cmd := exec.Command("sreport", "cluster", "utilization", "-nP", "--tres", "billing")
	out, err := cmd.Output()
	if err != nil {
		return
	}

	cols := strings.Split(strings.TrimSpace(string(out)), "|")
	if len(cols) != 8 {
		err = fmt.Errorf("bad sreport output")
		return
	}

	name := cols[0]
	size, err := strconv.Atoi(cols[7])
	if err != nil {
		return
	}
	size /= 24 * 60

	unit := "cores"
	if name == "mist" {
		unit = "GPUs"
	}

	info.name = name
	info.size = size
	info.unit = unit
	return
}

func prettyFloat(f float64) string {
	if math.Abs(f) >= 1e3 {
		return fmt.Sprintf("%g", math.Round(f))
	}
	return fmt.Sprintf("%.3g", f)
}

func showPriorityForAccount(account string) error {
	cluster, err := getClusterInfo()
	if err != nil {
		return err
	}

	cmd := exec.Command("sshare", "-alnP", "-A", account)
	out, err := cmd.Output()
	if err != nil {
		return err
	}

	lines := strings.Split(strings.TrimSpace(string(out)), "\n")
	if len(lines) < 2 {
		return fmt.Errorf("bad account: %s", account)
	}

	// account info
	cols := strings.Split(lines[0], "|")
	normShare, err := strconv.ParseFloat(cols[3], 64)
	if err != nil {
		return err
	}
	levelFS, err := strconv.ParseFloat(cols[8], 64)
	if err != nil {
		return err
	}
	acctUsagePercent := 100 / levelFS
	acctSize := normShare * float64(cluster.size)
	fmt.Printf("Account %s has used %s%% of its rolling %s allocation (~%s %s).\n\n",
		account, prettyFloat(acctUsagePercent), cluster.name, prettyFloat(acctSize), cluster.unit)

	// users
	lines = lines[1:]
	users := make([]struct {
		username     string
		usagePercent float64
	}, len(lines))
	for i, line := range lines {
		cols := strings.Split(line, "|")
		usage, err := strconv.ParseFloat(cols[6], 64)
		if err != nil {
			return err
		}

		users[i].username = cols[1]
		users[i].usagePercent = 100 * usage
	}

	sort.Slice(users, func(i, j int) bool {
		return users[i].usagePercent > users[j].usagePercent
	})

	tw := tabwriter.NewWriter(os.Stdout, 0, 0, 2, ' ', tabwriter.AlignRight)
	fmt.Fprintf(tw, "    User\tShare of recent usage\t\n")
	fmt.Fprintf(tw, "--------\t---------------------\t\n")
	for _, user := range users {
		if user.usagePercent > 0.1 {
			fmt.Fprintf(tw, "%s\t%.1f%%\t\n", user.username, user.usagePercent)
		}
	}
	tw.Flush()
	fmt.Println()
	return nil
}

func fatal(err error) {
	fmt.Println(err)
	os.Exit(1)
}

func main() {
	var account, username string
	var err error

	if len(os.Args) > 1 {
		userOrAccount := os.Args[1]
		if isUser(userOrAccount) {
			username = userOrAccount
		} else {
			account = userOrAccount
		}
	} else {
		username = os.Getenv("USER")
	}

	if account == "" {
		account, err = getDefaultAccountForUser(username)
		if err != nil {
			fatal(err)
		}
	}

	err = showPriorityForAccount(account)
	if err != nil {
		fatal(err)
	}

	os.Exit(0)
}
